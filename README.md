# eventuous-tests
Web application implemented with [Eventuous](https://eventuous.dev/) which uses event sourcing as persistence mechanism with Event Store DB, and catch-up subscriptions for read models with Event Store DB and Mongo DB.

## Requirements
- SDK .NET 7
- Event Store DB (as a Docker container. See below)
- Mongo DB (as a Docker container. See below)
- Hosts file with the following entries (`/etc/hosts` in Unix based OS and at `C:\Windows\System32\drivers\etc\hosts` in Windows.) to give alias to Event Store DB and Mongo DB in local as they have in GitLab CI/CD services
  ```
  # Local servers
  127.0.0.1   eventstoredb
  127.0.0.1   mongodb
  ```
  
### Event Store DB
Spin up an Event Store DB container (in-memory persistence) with
```
docker run \
    --name esdb \
    -p 2113:2113 \
    -p 1113:1113 \
    eventstore/eventstore:22.10.0-buster-slim \
    --insecure \
    --enable-atom-pub-over-http \
    --mem-db=True
```

### Mongo DB
Spin up a Mongo DB container (in-memory persistence) with
```
docker run \
    --name mongo \
    --mount type=tmpfs,destination=/data/db \
    -e MONGO_INITDB_ROOT_USERNAME=root \
    -e MONGO_INITDB_ROOT_PASSWORD=dummy \
    -p 27017:27017 \
    mongo:4.0
```
NOTE: The version is 4.0 to make it compatible with AWS DocumentDb

## Deterministic strategy
In order to run tests in parallel and in a deterministic way, every integration test execution will have its own TestServer instance, and this TestServer will override some DI registrations to:
1- Have EventStoreDB unique streams by appending a TestId to the stream name.
2- Have MongoDb create unique databases with name as TestId, and removing the database when finishing that test.

## How to run
Run the app, it will have swagger available at http://localhost:5001/swagger
Run tests with `dotnet test`. Observe CI/CD pipeline.

## How to reproduce the problem
There are 2 test projects:
1. `TestWithoutTestServer`: which doesn't use any TestServer and always runs in parallel
2. `Modular.Api.FunctionalTests`: which uses a TestServer and runs in parallel or sequentially depending on `Properties/AssemblyInfo.cs`

Both have a simple unique test repeated 100 times.

The `Modular.Api.FunctionalTests` is problematic.
- When running locally in parallel and TestServer is not disposed, it runs OK.
- When running locally in parallel and TestServer is disposed, it gets stuck, no exception. It seems `IWebHost` cannot be disposed despite explicitly stopping `IHostedService`.
- When running in GitLab CI/CD runner, in parallel and TestServer is not disposed, it gets stuck, no exception.
- When running in GitLab CI/CD runner, sequentially and TestServer is not disposed, it runs OK.
- When running in GitLab CI/CD runner, in parallel and TestServer is disposed.. not even attempted.

The workaround could be never to dispose the TestServer, and to locally run in parallel and in CI/CD pipeline run sequentially.