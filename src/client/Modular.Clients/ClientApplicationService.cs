using Eventuous;
using Modular.Clients.Domain;
using static Modular.Clients.Commands.ClientCommands;
// ReSharper disable ClassNeverInstantiated.Global

namespace Modular.Clients;

public class ClientApplicationService
    : ApplicationService<Client, ClientState, ClientId>
{
    public ClientApplicationService(IAggregateStore store, Services.HashPbkdf2 hashPbkdf2Service)
        : base(store)
    {
        OnNew<CreateClient>(
            cmd => new ClientId(cmd.Id),
            (client, cmd) => client.CreateClient(cmd.Id, cmd.DisplayName, cmd.AdminEmail));
        
        OnExisting<UpdateClientAdminCredentials>(
            cmd => new ClientId(cmd.Id),
            (client, cmd) => client.UpdateAdminCredentials(cmd.Password, hashPbkdf2Service));
    }
}