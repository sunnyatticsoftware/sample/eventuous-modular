using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Modular.Api.Controllers;

[Route("health")]
[AllowAnonymous]
public class HealthController
    : ControllerBase
{
    private readonly IWebHostEnvironment _environment;
    private readonly IConfiguration _configuration;

    public HealthController(
        IWebHostEnvironment environment,
        IConfiguration configuration)
    {
        _environment = environment;
        _configuration = configuration;
    }
        
    /// <summary>
    /// Gets service's health
    /// </summary>
    [HttpGet]
    public IActionResult Get()
    {
        var magic = _configuration.GetValue<string>("MagicValue");
        var environmentName = _environment.EnvironmentName;
        var currentUtcDateTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
        var assemblyName = typeof(Program).Assembly.GetName();
        var result = $"Assembly={assemblyName}, Environment={environmentName}, CurrentUtcTime={currentUtcDateTime}, Magic={magic}";
        return Ok(result);
    }
}
