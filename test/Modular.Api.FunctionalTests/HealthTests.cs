using FluentAssertions;
using Modular.Api.FunctionalTests.TestSupport;
using System.Net;
using Xunit.Abstractions;

namespace Modular.Api.FunctionalTests;

public class HealthTests
    : IntegrationTest
{
    private readonly ITestOutputHelper _testOutputHelper;

    public HealthTests(ITestOutputHelper testOutputHelper)
    {
        _testOutputHelper = testOutputHelper;
    }
    
    [Theory(DisplayName = "It should work")]
    [Repeat(50)]
    public async Task Health(int iteration)
    {
        _testOutputHelper.WriteLine($"Iteration {iteration}"); 
        var result = await HttpClient.GetAsync("health");
        result.StatusCode.Should().Be(HttpStatusCode.OK);
        var bodyJson = await result.Content.ReadAsStringAsync();
        bodyJson.Should().Contain("Environment=Development");
    }
}