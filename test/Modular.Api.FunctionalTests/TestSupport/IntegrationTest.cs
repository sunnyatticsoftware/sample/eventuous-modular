using Eventuous;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Modular.Api.FunctionalTests.TestSupport.Extensions;
using Modular.Clients.Domain;
using MongoDB.Driver;

namespace Modular.Api.FunctionalTests.TestSupport;

public abstract class IntegrationTest
    : IDisposable, IAsyncDisposable
{
    public HttpClient HttpClient { get; }
    public string TestId { get; }
    
    private readonly Func<Task> _cleanTask;

    protected IntegrationTest()
    {
        TestId = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, 6);
        var webApplicationFactory =
            new WebApplicationFactory<Program>()
                .WithWebHostBuilder(webHostBuilder =>
                    webHostBuilder
                        .ConfigureAppConfiguration((hostingContext, configBuilder) =>
                            configBuilder.Sources.Where(s => s is FileConfigurationSource).ToList()
                                .ForEach(s => ((FileConfigurationSource) s).ReloadOnChange = false))
                        .UseEnvironment("Development")
                        .ConfigureTestServices(ConfigureTestServices));
        
        HttpClient = webApplicationFactory.CreateClient();
        
        _cleanTask = async () =>
        {
            var services = webApplicationFactory.Services;
             // Cleaning tasks
             var mongoClient = services.GetRequiredService<MongoClient>();
             await mongoClient.DropDatabaseAsync(TestId);

             // var hostedServices = services.GetRequiredService<IEnumerable<IHostedService>>();
             // var cancellationToken = new CancellationToken();
             // var hostedServicesStopTasks = hostedServices.Select(x => x.StopAsync(cancellationToken));
             // await Task.WhenAll(hostedServicesStopTasks);
             //
             // var eventStoreClient = services.GetRequiredService<EventStoreClient>();
             // await eventStoreClient.DisposeAsync();
             
             //webApplicationFactory.Dispose();
         };
    }


    protected virtual void ConfigureTestServices(IServiceCollection services)
    {
        // Unique streams
        _ = services.RemoveRegisteredService<StreamNameMap>();

        var streamNameMapTest = new StreamNameMap();
        streamNameMapTest.Register<ClientId>(
            clientId => new StreamName($"Client-{clientId.Value}-{TestId}"));
        services.AddSingleton(streamNameMapTest);

        // Unique database
        _ = services.RemoveRegisteredService<IMongoDatabase>();
        services.AddSingleton(
            sp =>
            {
                var client = sp.GetRequiredService<MongoClient>();
                var mongoDatabase = client.GetDatabase(TestId);
                return mongoDatabase;
            });
    }

    public void Dispose()
    {
        _ = Clean();
    }

    public async ValueTask DisposeAsync()
    {
        await Clean();
    }

    private async Task Clean()
    {
        await _cleanTask();
    }
}