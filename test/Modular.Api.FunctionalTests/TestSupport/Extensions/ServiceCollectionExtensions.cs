using Microsoft.Extensions.DependencyInjection;

namespace Modular.Api.FunctionalTests.TestSupport.Extensions;

public static class ServiceCollectionExtensions
{
    public static bool RemoveRegisteredService<TService>(this IServiceCollection services)
    {
        var serviceDescriptor = services.SingleOrDefault(d => d.ServiceType == typeof(TService));
        if (serviceDescriptor != null)
        {
            return services.Remove(serviceDescriptor);
        }

        return false;
    }
}