using FluentAssertions;
using TestWithoutTestServer.TestSupport;
using Xunit.Abstractions;

namespace TestWithoutTestServer;

public class DummyTests
{
    private readonly ITestOutputHelper _testOutputHelper;

    public DummyTests(ITestOutputHelper testOutputHelper)
    {
        _testOutputHelper = testOutputHelper;
    }

    [Theory(DisplayName = "It should work")]
    [Repeat(100)]
    public void Health(int iteration)
    {
        _testOutputHelper.WriteLine($"Iteration {iteration}");
        iteration.Should().BePositive();
    }
}